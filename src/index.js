import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import{Form} from './components/Form';

ReactDOM.render(<Form />, document.getElementById('root'));
registerServiceWorker();
