import React, {Component} from 'react';

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {value: "", response: ""};
        this.changeValue = this.changeValue.bind(this);
        this.chooseContent = this.chooseContent.bind(this);
    }

    changeValue(event) {
        this.setState({value: event.target.value});
    }

    chooseContent(){
        switch(this.state.value) {
            case "успешно" :
                return <div className="success">Success</div>
                    break;

            case "ошибка" :
              return <div className="error">Error</div>
            break;

            case "подождите" :
                return <div className="wait">Wait</div>
            break;

            default:
                return null;
        }
    };

    render() {
        return (
            <div className="main-cont">
                <form>
                    <span className="form-title">Whois query:</span>
                    <input onChange={this.changeValue} value={this.state.value} type="text" />
                    <input type="submit" value="Submit" />
                    <div className="results">{this.chooseContent()}</div>
                </form>
            </div>
        )
    }
};

export {Form}